#######################
# Setup and create the Nginx image
# Runs as a non root user
# Loads the adjacent nginx.conf file
# Dynamically accecpts an FQDN
# Generates a self signed certificate for Nginx
#######################
ARG nginxversion=latest
FROM nginx:$nginxversion as build
ARG CUID=1002
ARG CGID=1003
ARG domain="localhost"

LABEL maintainer="Tyler Radlick / tyler@infinitecode.dev"

# Remove default configuration overrides
RUN rm -rf /docker-entrypoint.d/* \
    && rm -rf /etc/nginx/conf.d/default.conf

# Create appuser
RUN set -x \
    && addgroup --system -gid ${CGID} appuser \
    && adduser --system --disabled-login --ingroup appuser --no-create-home --home /nonexistent --gecos "nginx user" --shell /bin/false -uid $CUID appuser

# Setup Directories
RUN mkdir -p /app \
    && mkdir -p /etc/nginx/ssl

# Setup Directory Permissions
RUN chown -R appuser:appuser /app

# nginx user must own the cache directory to write cache
RUN chown -R appuser:0 /var/cache/nginx \
    && chmod -R g+w /var/cache/nginx

RUN openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048

# Get the Nginx Config File
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf

# Update the domain name for the Nginx config file
RUN sed -i 's/CHANGE_ME/'${domain}'/g' /etc/nginx/nginx.conf \
    && sed -i 's/MAXAGEHSTS/'${maxage}'/g' /etc/nginx/nginx.conf


# Check if a certificate matching the supplied domain exists. If so, skip creating it, if not, create a new one
RUN openssl req -new -newkey rsa:8192 -days 365 -nodes -x509 -subj "/C=US/ST=Texas/L=Austin/O=IT/CN=$domain" -keyout /etc/nginx/ssl/$domain.key  -out /etc/nginx/ssl/$domain.crt

RUN chown -R appuser:appuser /etc/nginx/ssl

# Switch to non-privligged used
USER appuser

# Set runtime working directory
WORKDIR /app

# Expose http and https ports
EXPOSE 8080 8443

STOPSIGNAL SIGTERM

CMD ["nginx"]
