#!/usr/bin/env sh

source "$(dirname $0)/../../.env"

docker exec -it "$APP_IMAGE_NAME"-horizon supervisorctl -c /etc/supervisor/conf.d/supervisord.conf stop all