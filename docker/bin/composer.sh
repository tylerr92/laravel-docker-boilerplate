#!/usr/bin/env sh

source "$(dirname $0)/../../.env"

CONTAINER=$APP_IMAGE_NAME-php-fpm

docker exec -it "$CONTAINER" composer $*