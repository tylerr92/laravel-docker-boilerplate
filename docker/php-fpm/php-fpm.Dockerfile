#######################
# Setup and create the php-fpm image
# Runs as a non root user
# Loads the specificed php.ini file
# Uses PHP-FPM with default support for all Laravel 7 requirements
#######################
ARG phpversion=7.4
FROM php:$phpversion-fpm as build
ARG CUID=1000
ARG CGID=1000
ARG XDEBUG_ENABLED=disabled
ARG phpversion=7.4
ARG nodeversion=latest
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.9/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64

LABEL maintainer="Tyler Radlick / tyler@infinitecode.dev"

RUN set -x \
        && addgroup --system -gid $CGID appuser \
        && adduser --system --disabled-login --ingroup appuser --gecos "php-fpm user" --shell /bin/false -uid $CUID appuser

RUN set -x \
    && addgroup --system node \
    && adduser --system --disabled-login --ingroup node --gecos "node user" --shell /bin/false node \
    && usermod -a -G appuser node

# Setup Directories
RUN mkdir -p /app \
    && mkdir -p /cron

# Setup Directory Permissions
RUN chown -R appuser:appuser /app \
    && chown -R appuser:appuser /cron

# Enable debian repos and install requirements
RUN mv /etc/apt/preferences.d/no-debian-php ~/no-debian-php \
    && apt-get update \
    && apt-get install -y git nano python3-pip build-essential python-all-dev python-setuptools --no-install-recommends \
    && mv ~/no-debian-php /etc/apt/preferences.d/no-debian-php

# Install PHP Extenstions - Doc: https://github.com/mlocati/docker-php-extension-installer
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions bcmath gd imap intl ldap mcrypt opcache soap tidy xmlrpc zip pcntl redis pdo_mysql mysqli opencensus

# If in development mode install xdebug
RUN if [ "$XDEBUG_ENABLED" = TRUE ] ; then install-php-extensions xdebug; fi ;

# Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer

# Install Node version manager and node
RUN cd /usr/local/bin/ \
    && curl -L https://raw.githubusercontent.com/tj/n/master/bin/n -o n \
    && chmod +x n \
    && n ${nodeversion} \
    && npm -v \
    && node -v

# Install supercronic
RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic \
 && chown appuser:appuser /usr/local/bin/supercronic

# Install supervisor
RUN pip install supervisor --no-cache-dir

# Copy in versioned ini file
COPY docker/php-fpm/php$phpversion.ini /usr/local/etc/php/conf.d/php.ini

# Copy in php-fpm config
COPY docker/php-fpm/php-fpm-pool.conf /usr/local/etc/php-fpm.d/www.conf

# Copy in crontab config file
COPY --chown=appuser:appuser docker/php-fpm/crontab /cron/crontab

# Copy in the supervisor config
COPY --chown=appuser:appuser docker/php-fpm/supervisor.conf /etc/supervisor/conf.d/supervisord.conf

# Switch to non-privlliged user
USER appuser

# Set working directory
WORKDIR /app

# Run the startup script and then run the containers command
CMD ["/bin/bash", "-c", "php-fpm"]
